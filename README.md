#Telecom provider coding test project

## API documentation
#####List all of the phone numbers  

	`GET phone-numbers/`
	
#####List all of the phone numbers of the specified user  

	`GET phone-numbers/{id}`

*path params:*  
  * **id:** the id of the specified user  
  
#####Add a new phone number to the specified user 

	`GET phone-numbers/{id}/{newPhoneNumber}`

*path params:*  
  * **id:** the id of the specified user  
  * **newPhoneNumber:** the new phone number to give  