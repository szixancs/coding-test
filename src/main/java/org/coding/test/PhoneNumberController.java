package org.coding.test;

import java.util.List;
import java.util.NoSuchElementException;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PhoneNumberController {

    private PhoneService service;

    public PhoneNumberController(PhoneService service) {
	this.service = service;
    }

    @RequestMapping(value = "/phone-numbers/", method = RequestMethod.GET)
    @ResponseBody
    List<String> get() {
	return service.getAllPhoneNumbers();

    }

    @RequestMapping(value = "/phone-numbers/{id}", method = RequestMethod.GET)
    @ResponseBody
    List<String> get(@PathVariable("id") long id) {
	try {
	    return service.getPhoneNumbersOf(id);
	} catch (NoSuchElementException nsee) {
	    throw new CustomerNotFoundException(nsee);
	} catch (IllegalArgumentException iae) {
	    throw new InvalidInputFoundException(iae);
	}

    }

    @RequestMapping(value = "/phone-numbers/{id}/{phoneNumber}", method = RequestMethod.PUT)
    @ResponseBody
    void get(@PathVariable("id") long id, @PathVariable("phoneNumber") String phoneNumber) {
	try {
	    service.addPhoneNumber(id, phoneNumber);
	} catch (NoSuchElementException nsee) {
	    throw new CustomerNotFoundException(nsee);
	} catch (IllegalArgumentException iae) {
	    throw new InvalidInputFoundException(iae);
	}

    }

    @ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Customer with the given id is not found!")
    static class CustomerNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 7331067014207558819L;

	public CustomerNotFoundException(Throwable cause) {
	    super(cause);
	}
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    static class InvalidInputFoundException extends RuntimeException {

	private static final long serialVersionUID = -8025023222222244379L;

	public InvalidInputFoundException(Throwable cause) {
	    super(cause);
	}
    }
}
