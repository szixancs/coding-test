package org.coding.test.customer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

@Component
public class CustomerRepository {

    private Set<Customer> repository = new HashSet<>();

    {
	Customer customer = new Customer("Jhon Smith");
	customer.addPhoneNumber("(111)1234567");
	customer.addPhoneNumber("(111)1234568");
	customer.addPhoneNumber("(111)1234569");
	repository.add(customer);

	customer = new Customer("Jack Smith");
	customer.addPhoneNumber("(123)2222222");
	customer.addPhoneNumber("(123)1111111");
	repository.add(customer);

	customer = new Customer("Jim Smith");
	customer.addPhoneNumber("(333)5557777");
	repository.add(customer);
    }

    public List<Customer> getAll() {
	return new ArrayList<>(repository);
    }

}
