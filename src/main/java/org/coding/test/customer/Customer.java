package org.coding.test.customer;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

public class Customer {

    private static AtomicLong idCounter = new AtomicLong();

    private long id;
    private String name;
    private Set<String> phoneNumbers;
    private static Pattern pattern = Pattern.compile("^\\((\\d{3})\\)\\d{7}$");

    public Customer(String name) {
	this.name = name;
	this.id = idCounter.getAndIncrement();
	phoneNumbers = new HashSet<>();
    }

    /**
     * Add new phone number to the customer. The valid phone number format (d =
     * digit): "(ddd)ddddddd".
     * 
     * @param newPhoneNumber
     * @return false if the number have been added yet
     * @throws IllegalArgumentException
     *             if the format of the given number is not valid
     */
    public boolean addPhoneNumber(String newPhoneNumber) {
	if (newPhoneNumber == null || !pattern.matcher(newPhoneNumber).matches()) {
	    throw new IllegalArgumentException(String.format(
		    "The format of the given phone number [%s] is not valid. The valid format is '(ddd)ddddddd'",
		    newPhoneNumber));
	}
	return phoneNumbers.add(newPhoneNumber);
    }

    public List<String> getPhoneNumbers() {
	return new ArrayList<>(phoneNumbers);
    }

    public long getId() {
	return id;
    }
}
