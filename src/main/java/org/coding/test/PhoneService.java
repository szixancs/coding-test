package org.coding.test;

import java.util.Collections;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.coding.test.customer.Customer;
import org.coding.test.customer.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PhoneService {

    static final String MSG_INVALID_ID = "Invalid id: ";
    private CustomerRepository customerRepository;

    @Autowired
    public PhoneService(CustomerRepository customerRepository) {
	this.customerRepository = customerRepository;
    }

    /**
     * Get all phone numbers from the repository.
     * 
     * @return all phone numbers from the repository.
     */
    public List<String> getAllPhoneNumbers() {
	return getAllCustomers().stream().flatMap(c -> c.getPhoneNumbers().stream()).collect(Collectors.toList());
    }

    /**
     * Returns with the list of phone numbers of the specified customer.
     * 
     * @param id
     *            the id of the customer, it cannot be a negative number
     * @return the list of phone numbers
     * @throws IllegalArgumentException
     *             if the given id is less then zero
     * @throws NoSuchElementException
     *             if the customer does not exist
     * 
     */
    public List<String> getPhoneNumbersOf(long id) {
	return getCustomer(id).getPhoneNumbers();
    }

    /**
     * Add a new number to an existing customer.
     * 
     * @param id
     *            the id of the customer, it cannot be a negative number
     * @param newPhoneNumber
     *            the new number to add
     * @return if the activation was successful
     * 
     * @throws IllegalArgumentException
     *             if the given id is less then zero or the phone number format
     *             was wrong
     * @throws NoSuchElementException
     *             if the customer does not exist
     */
    public boolean addPhoneNumber(long id, String newPhoneNumber) {
	return getCustomer(id).addPhoneNumber(newPhoneNumber);
    }

    private Customer getCustomer(long id) {
	if (id < 0) {
	    throw new IllegalArgumentException(MSG_INVALID_ID + id);
	}

	Optional<Customer> customer = getAllCustomers().stream().filter(c -> c.getId() == id).findFirst();

	if (!customer.isPresent()) {
	    throw new NoSuchElementException();
	}
	return customer.get();
    }

    private List<Customer> getAllCustomers() {
	List<Customer> customers = customerRepository.getAll();
	if (customers == null) {
	    return Collections.emptyList();
	}
	return customers;
    }
}
