package org.coding.test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import org.coding.test.customer.Customer;
import org.coding.test.customer.CustomerRepository;
import org.hamcrest.collection.IsEmptyCollection;
import org.hamcrest.core.IsNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PhoneServiceTest {

    private static final String TEST_NUMBER_1 = "(003)6301111";
    private static final String TEST_NUMBER_2 = "(006)3011112";
    private static final String TEST_NUMBER_3 = "(000)1111113";
    private static final String TEST_NUMBER_4 = "(363)0111114";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    PhoneService underTest;

    @Mock
    CustomerRepository mockCustomerRepository;

    @Test
    public void testWhenGetAllWhenGetNullFromRepositoryThenReturnEmptyList() {
	when(mockCustomerRepository.getAll()).thenReturn(null);

	List<String> result = underTest.getAllPhoneNumbers();

	assertThat(result, new IsEmptyCollection<String>());
    }

    @Test
    public void testGetAllWhenGetEmptyListFromRepositoryThenReturnEmptyList() {
	when(mockCustomerRepository.getAll()).thenReturn(new ArrayList<>());

	List<String> result = underTest.getAllPhoneNumbers();

	assertThat(result, new IsEmptyCollection<String>());
    }

    @Test
    public void testGetAllWhenGetAllCustomerFromRepositoryThenReturnPhoneNumbers() {
	List<Customer> testData = new ArrayList<>();

	Customer customer = new Customer("Jhon Smith");
	customer.addPhoneNumber(TEST_NUMBER_1);
	customer.addPhoneNumber(TEST_NUMBER_2);
	customer.addPhoneNumber(TEST_NUMBER_3);
	testData.add(customer);

	customer = new Customer("Jack Smith");
	customer.addPhoneNumber(TEST_NUMBER_4);
	testData.add(customer);

	customer = new Customer("Jim Smith");
	testData.add(customer);

	when(mockCustomerRepository.getAll()).thenReturn(testData);

	List<String> result = underTest.getAllPhoneNumbers();

	assertThat(result, hasSize(4));
	assertThat(result, hasItem(TEST_NUMBER_1));
	assertThat(result, hasItem(TEST_NUMBER_2));
	assertThat(result, hasItem(TEST_NUMBER_3));
	assertThat(result, hasItem(TEST_NUMBER_4));
    }

    @Test
    public void testGetOfWhenGetNullFromRepositoryThenThrowException() {
	expectedException.expect(NoSuchElementException.class);
	when(mockCustomerRepository.getAll()).thenReturn(new ArrayList<>());

	List<String> result = underTest.getPhoneNumbersOf(0);

	assertThat(result, new IsNull<>());
    }
    
    @Test
    public void testGetOfWhenGetNonExistingIdThenThrowException() {
	expectedException.expect(NoSuchElementException.class);
	when(mockCustomerRepository.getAll()).thenReturn(new ArrayList<>());

	underTest.getPhoneNumbersOf(155);
    }

    @Test
    public void testGetOfWhenGetNegativeIdThenThrowException() {
	expectedException.expect(IllegalArgumentException.class);
	expectedException.expectMessage(containsString(PhoneService.MSG_INVALID_ID));

	underTest.getPhoneNumbersOf(-1);
    }
    
    @Test
    public void testGetOfWhenGetExistingIdThenReturnPhoneNumbers() {
	List<Customer> testData = new ArrayList<>();

	Customer customer = new Customer("Jhon Smith");
	customer.addPhoneNumber(TEST_NUMBER_1);
	customer.addPhoneNumber(TEST_NUMBER_3);

	testData.add(customer);

	customer = new Customer("Jack Smith");
	customer.addPhoneNumber(TEST_NUMBER_4);
	customer.addPhoneNumber(TEST_NUMBER_2);
	long testId = customer.getId();
	testData.add(customer);

	customer = new Customer("Jim Smith");
	testData.add(customer);

	when(mockCustomerRepository.getAll()).thenReturn(testData);

	List<String> result = underTest.getPhoneNumbersOf(testId);

	assertThat(result, hasSize(2));
	assertThat(result, hasItem(TEST_NUMBER_2));
	assertThat(result, hasItem(TEST_NUMBER_4));
    }
    
    @Test
    public void testGetOfWhenGetIdOfACustomerWithoutPhoneNumberThenReturnEmptyList() {
	List<Customer> testData = new ArrayList<>();

	Customer customer = new Customer("Jhon Smith");
	customer.addPhoneNumber(TEST_NUMBER_1);
	customer.addPhoneNumber(TEST_NUMBER_2);
	customer.addPhoneNumber(TEST_NUMBER_3);
	testData.add(customer);

	customer = new Customer("Jack Smith");
	customer.addPhoneNumber(TEST_NUMBER_4);
	testData.add(customer);

	customer = new Customer("Jim Smith");
	long testId = customer.getId();
	testData.add(customer);

	when(mockCustomerRepository.getAll()).thenReturn(testData);

	List<String> result = underTest.getPhoneNumbersOf(testId);

	assertThat(result, new IsEmptyCollection<String>());
    }
    
    @Test
    public void testAddPhoneNumberWhenGetNegativeIdThenThrowException() {
	expectedException.expect(IllegalArgumentException.class);
	expectedException.expectMessage(containsString(PhoneService.MSG_INVALID_ID));

	underTest.addPhoneNumber(-1, TEST_NUMBER_1);
    }
    
    @Test
    public void testAddPhoneNumberWhenGetInvalidPhoneNumberThenThrowException() {
	List<Customer> testData = new ArrayList<>();
	Customer customer = new Customer("Jim Smith");
	long testId = customer.getId();
	testData.add(customer);
	
	when(mockCustomerRepository.getAll()).thenReturn(testData);
	expectedException.expect(IllegalArgumentException.class);
	expectedException.expectMessage(containsString("not valid"));

	underTest.addPhoneNumber(testId, "(123)");
    }
    
    @Test
    public void testAddPhoneNumberWhenGetValidDataThenAddNumber() {
	List<Customer> testData = new ArrayList<>();
	Customer customer = new Customer("Jim Smith");
	long testId = customer.getId();
	testData.add(customer);
	
	when(mockCustomerRepository.getAll()).thenReturn(testData).thenReturn(testData);

	boolean result = underTest.addPhoneNumber(testId, TEST_NUMBER_1);
	List<String> numberList = underTest.getPhoneNumbersOf(testId);
	
	assertThat("Returns true", result);
	assertThat(numberList, hasSize(1));
	assertThat(numberList, hasItem(TEST_NUMBER_1));
    }
    
    @Test
    public void testAddPhoneNumberMoreTimesWhenGetValidDataThenReturnFalse() {
	List<Customer> testData = new ArrayList<>();
	Customer customer = new Customer("Jim Smith");
	customer.addPhoneNumber(TEST_NUMBER_1);
	long testId = customer.getId();
	testData.add(customer);
	
	when(mockCustomerRepository.getAll()).thenReturn(testData).thenReturn(testData);

	boolean result = underTest.addPhoneNumber(testId, TEST_NUMBER_1);
	List<String> numberList = underTest.getPhoneNumbersOf(testId);
	
	assertThat("Returns false", !result);
	assertThat(numberList, hasSize(1));
	assertThat(numberList, hasItem(TEST_NUMBER_1));
    }

}
