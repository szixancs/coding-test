package org.coding.test.customer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CustomerTest {

    private static final String TEST_NUMBER = "(123)4567890";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    
    private Customer underTest;
    
    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void testAddNullPhoneNumberThrowException() {
	underTest = new Customer("Jhon Smith");
	expectedException.expect(IllegalArgumentException.class);
	
	underTest.addPhoneNumber(null);
    }
    
    @Test
    public void testAddEmptyPhoneNumberThrowException() {
	underTest = new Customer("Jhon Smith");
	expectedException.expect(IllegalArgumentException.class);
	
	underTest.addPhoneNumber("");
    }
    
    @Test
    public void testAddInvalidPhoneNumberThrowException() {
	underTest = new Customer("Jhon Smith");
	expectedException.expect(IllegalArgumentException.class);
	
	underTest.addPhoneNumber("1234567890");
    }
    
    @Test
    public void testAddTooLongPhoneNumberThrowException() {
	underTest = new Customer("Jhon Smith");
	expectedException.expect(IllegalArgumentException.class);
	
	underTest.addPhoneNumber("(123)45678900");
    }
    
    @Test
    public void testAddTooShortPhoneNumberThrowException() {
	underTest = new Customer("Jhon Smith");
	expectedException.expect(IllegalArgumentException.class);
	
	underTest.addPhoneNumber("(123)456789");
    }
    
    @Test
    public void testAddValidPhoneNumberThenItIsSaved() {
	underTest = new Customer("Jhon Smith");
	
	boolean result = underTest.addPhoneNumber(TEST_NUMBER);
	List<String> resultList = underTest.getPhoneNumbers();
	
	
	assertThat("Returns true", result);
	assertThat(resultList, hasSize(1));
	assertThat(resultList, hasItem(TEST_NUMBER));
    }
    
    @Test
    public void testAddValidPhoneNumberMoreTimesThenItIsSavedOnce() {
	underTest = new Customer("Jhon Smith");
	underTest.addPhoneNumber(TEST_NUMBER);
	
	boolean result = underTest.addPhoneNumber(TEST_NUMBER);
	List<String> resultList = underTest.getPhoneNumbers();
	
	assertThat("Returns false", !result);
	assertThat(resultList, hasSize(1));
	assertThat(resultList, hasItem(TEST_NUMBER));
    }

}
